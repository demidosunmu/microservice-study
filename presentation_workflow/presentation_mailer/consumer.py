import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


while True:
    try:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters("rabbitmq")
        )
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")

        def process_approval(ch, method, properties, body):
            print("Test")
            data = json.loads(body)
            email = data["presenter_email"]
            name = data["presenter_name"]
            title = data["title"]
            send_mail(
                        "Your presentation has been accepted",
                        f"{name}, we're happy to tell you that your presentation {title} has been accepted",
                        "admin@conferece.go",
                        [email],
                        fail_silently=False,
                    )

        def process_rejection(ch, method, properties, body):
            data = json.loads(body)
            email = data["presenter_email"]
            name = data["presenter_name"]
            title = data["title"]
            send_mail(
                        "Your presentation has been rejected",
                        f"{name}, we're sorry to tell you that your presentation {title} has been rejected",
                        "admin@conferece.go",
                        [email],
                        fail_silently=False,
                    )

        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()

    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
